from opcua import Client
from opcua import Node
from opcua import ua
from lxml import etree

def extract_address_space(server_url, output_file):
    # Connect to the OPC UA server
    client = Client(server_url)

    client.connect()

    # Get the address space of the server
    address_space = client.get_objects_node()

    # Export the address space as Nodeset2 XML
    address_space.export_xml(output_file, 2)

    print(f"Address space extracted successfully and saved to {output_file}")

    # Disconnect from the server
    client.disconnect()

if __name__ == "__main__":
    server_url = "opc.tcp://admin:admin@localhost:4840"  # Change this to the URL of your OPC UA server
    output_file = "address_space.xml"  # Change this to the desired output file path
    extract_address_space(server_url, output_file)
