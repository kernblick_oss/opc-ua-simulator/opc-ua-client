# OPC UA Client

The opc ua client connects to a OPC UA server and extracts the address space in different formats.

Use the following environment variables to adjust the client's output:

 - **ENDPOINT**: The opc ua endpoint address. Default: ``opc.tcp://localhost:4840/``
 - **USER**: The username to use for authentication. If it contains ``anon``, no user will be used for authentication. Default: ``admin``
 - **PASS**: The password to use for authentication. Default: ``admin``
 - **POLICY**: The security policy to use during connect: ``None``, ``Basic128Rsa1Basic256Sha256``, ``Basic256``, ``Aes128_Sha256_RsaOaep``, ``Aes256_Sha256_RsaPss``. Default: ``None``
 - **MODE**: The output requested: ``CSV``, ``SIM_VARIABLES``, ``ACTIONS``, ``OPCUA_VARIABLES``, ``OPCUA_ROOT_VARIABLES``

Build the OPC UA client as follows:

```bash
mvn clean install
```

Run the OPC UA client as follows:

```bash
java -jar target\opcua-client-x.x.x-jar-with-dependencies.jar -help
```
