package de.kernblick.opcua.client;

import java.util.Map;

/*
 * Copyright (c) 2021 the Eclipse Milo Authors
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */


import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.identity.AnonymousProvider;
import org.eclipse.milo.opcua.sdk.client.api.identity.IdentityProvider;
import org.eclipse.milo.opcua.sdk.client.api.identity.UsernameProvider;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;


public interface IOpcUaClient {

    default Boolean envContains(String key) {
        Map<String, String> env = System.getenv();
        for (String _key : env.keySet()) {
            if (_key.toLowerCase().equals(key.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    default String envGet(String key) {
        Map<String, String> env = System.getenv();
        for (String _key : env.keySet()) {
            if (_key.toLowerCase().equals(key.toLowerCase())) {
                return env.get(_key);
            }
        }
        return null;
    }

    default String getEndpointUrl() {
        if (this.envContains("endpoint")) {
            return this.envGet("endpoint");
        }
        return "opc.tcp://localhost:4840/";
    }

    default String getBrowseRoot() {
        if (this.envContains("root")) {
            return this.envGet("root");
        }
        return "i=85";
    }

    default Predicate<EndpointDescription> endpointFilter() {
        return e -> getSecurityPolicy().getUri().equals(e.getSecurityPolicyUri());
    }

    default SecurityPolicy getSecurityPolicy() {
        if (this.envContains("policy")) {
            if(this.envGet("policy").equals("None")) {
                return SecurityPolicy.None;
            }
            if(this.envGet("policy").equals("Basic128Rsa15")) {
                return SecurityPolicy.Basic128Rsa15;
            }
            if(this.envGet("policy").equals("Basic256Sha256")) {
                return SecurityPolicy.Basic256Sha256;
            }
            if(this.envGet("policy").equals("Basic256")) {
                return SecurityPolicy.Basic256;
            }
            if(this.envGet("policy").equals("Aes128_Sha256_RsaOaep")) {
                return SecurityPolicy.Aes128_Sha256_RsaOaep;
            }
            if(this.envGet("policy").equals("Aes256_Sha256_RsaPss")) {
                return SecurityPolicy.Aes256_Sha256_RsaPss;
            }
        }
        return SecurityPolicy.None;
    }

    default IdentityProvider getIdentityProvider() {
        String user = "admin";
        String pass = "admin";
        if (this.envContains("user")) { user = this.envGet("user"); }
        if (this.envContains("pass")) { pass = this.envGet("pass"); }

        if(user.startsWith("anon")) {
            return new AnonymousProvider();
        } else {
            return new UsernameProvider(user, pass);
        }
    }

    void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception;

}