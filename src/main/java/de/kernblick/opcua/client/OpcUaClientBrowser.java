/*
 * Copyright (c) 2019 the Eclipse Milo Authors
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package de.kernblick.opcua.client;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
import static org.eclipse.milo.opcua.stack.core.util.ConversionUtil.toList;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.bouncycastle.pqc.jcajce.provider.lms.LMSSignatureSpi.generic;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.nodes.UaNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.sdk.core.nodes.Node;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.ExpandedNodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.enumerated.BrowseDirection;
import org.eclipse.milo.opcua.stack.core.types.enumerated.BrowseResultMask;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.structured.BrowseDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.BrowseResult;
import org.eclipse.milo.opcua.stack.core.types.structured.ReferenceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpcUaClientBrowser implements IOpcUaClient {

    private enum Mode {
        CSV, SIM_VARIABLES, ACTIONS, OPCUA_VARIABLES, OPCUA_ROOT_VARIABLES, MODEL_DESIGN;
    }

    public static void main(String[] args) throws Exception {
        if (args.length>0 && (args[0].contains("help") || args[0].contains("?"))) {
            System.out.println("                            ");
            System.out.println("OPC UA Address Space Browser");
            System.out.println("============================");
            System.out.println("Use the following environment variables to adjust the client's output:");
            System.out.println(" - ENDPOINT: The opc ua endpoint address. Default: opc.tcp://localhost:4840/");
            System.out.println(" - USER: The username to use for authentication. If it contains 'anon', no user will be used for authentication. Default: admin");
            System.out.println(" - PASS: The password to use for authentication. Default: admin");
            System.out.println(" - POLICY: The security policy to use during connect: None, Basic128Rsa15, Basic256Sha256, Basic256, Aes128_Sha256_RsaOaep, Aes256_Sha256_RsaPss. Default: None");
            System.out.println(" - MODE: The output requested: CSV, SIM_VARIABLES, ACTIONS, OPCUA_VARIABLES, OPCUA_ROOT_VARIABLES");
            System.out.println("                            ");
        } else {
            OpcUaClientBrowser clientBrowser = new OpcUaClientBrowser();
            new OpcUaClientRunner(clientBrowser).run();
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        // synchronous connect
        logger.info("Connecting to OPC UA server.");
        client.connect().get();

        // start browsing at root folder
        // browseNode("", client, Identifiers.RootFolder, null);
        // browseNode("", client, Identifiers.ObjectNode, null);
        // browseNode("", client, NodeId.parse("ns=2;s=aut.FV.rActCutNr"), null);
        logger.info("Scanning node {}", this.getBrowseRoot());
        browseNode("", client, NodeId.parse(this.getBrowseRoot()), null);

        future.complete(client);
    }

    private List<ReferenceDescription> getChildren(OpcUaClient client, NodeId browseRoot) {
        BrowseDescription browse = new BrowseDescription(
            browseRoot,
            BrowseDirection.Forward,
            Identifiers.References,
            true,
            uint(NodeClass.Object.getValue() | NodeClass.Variable.getValue() | NodeClass.Method.getValue()),
            uint(BrowseResultMask.All.getValue())
        );


        try {
            BrowseResult browseResult = client.browse(browse).get();

            List<ReferenceDescription> references = toList(browseResult.getReferences());

            return references;
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Browsing nodeId={} failed: {}", browseRoot, e.getMessage(), e);
        }

        return null;
    }

    private String getType(OpcUaClient client, ExpandedNodeId expandedNodeId) {



        try {

            // Resolve the ExpandedNodeId to a NodeId
            NodeId nodeId = expandedNodeId.toNodeId(client.getNamespaceTable()).get();
            Node genericNode = client.getAddressSpace().getNode(nodeId);

            if(genericNode.getNodeClass().equals(NodeClass.Variable)) {

                UaVariableNode node = client.getAddressSpace().getVariableNode(nodeId);

                if(node.getDataType() != null) {
                    NodeId typeNodeId = node.getDataType();
                    UaNode typeNode = client.getAddressSpace().getNode(typeNodeId);
                    return typeNode.getBrowseName().getName();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";

    }


    private void browseNode(String indent, OpcUaClient client, NodeId browseRoot, String prefix) {

        // prefix for node name
        if(prefix == null) prefix = "";

        List<ReferenceDescription> children = getChildren(client, browseRoot);

        Set<String> ignore = new HashSet<>(Arrays.asList("ControlServer", "Types", "Views", "EURange", "Server"));
        // Set<String> ignore = new HashSet<>(Arrays.asList());

        for (int i = 0; i < children.size(); i++) {
            ReferenceDescription rd = children.get(i);
            boolean last = (i+1) == children.size();

            // only if we do not want to ignore node
            if(!ignore.contains(rd.getBrowseName().getName())) {

                // no child existing

                if(last) {
                    if(indent.length() >= 4) {
                        indent = indent.substring(0, indent.length() - 3) + "└──";
                    }
                }

                Optional<NodeId> nodeId = rd.getNodeId().toNodeId(client.getNamespaceTable());
                if(nodeId.isPresent()) {

                    /* Show a tree of the address space: */
                    // System.out.println(MessageFormat.format("{0} {1} {2}", indent, rd.getBrowseName().getName(), rd.getNodeId().toString(), rd.getTypeDefinition().getType().name()));

                    try {
                        Node genericNode = client.getAddressSpace().getNode(nodeId.get());

                        Boolean childrenVisted = false;

                        String indentationForXML = this.getMode().equals(Mode.MODEL_DESIGN) ? " ".repeat((indent.length()/3)*4) : "";

                        String rootReference = indentationForXML+"  <opc:References> <opc:Reference IsInverse=\"true\"> <opc:ReferenceType>ua:Organizes</opc:ReferenceType> <opc:TargetId>ua:ObjectsFolder</opc:TargetId> </opc:Reference> </opc:References>";

                        // only print variables
                        if(genericNode.getNodeClass().equals(NodeClass.Variable)) {
                            // print csv list of nodes for DIL/MESSAGE import
                            if(this.getMode().equals(Mode.CSV))
                                System.out.println(MessageFormat.format("{1};{2};{3}", indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId())));

                            // print list of variables for OPC UA Simulator
                            if(this.getMode().equals(Mode.SIM_VARIABLES))
                                System.out.println(MessageFormat.format("<variable name=\"{1}\" value=\"{2}\" />", indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId())));

                            // print list of opcua send events for the OPC UA Simulator
                            if(this.getMode().equals(Mode.ACTIONS))
                                System.out.println(MessageFormat.format("<opcua:send endpoint=\"opcUaEndpoint\" nodeid=\"$'{'{1}'}'\"> <message><data>{4}</data></message> </opcua:send>", indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), randomValue(getType(client, rd.getNodeId()))));

                            // print list of variables for OPC UA ModelDesign
                            if(this.getMode().equals(Mode.OPCUA_VARIABLES) || this.getMode().equals(Mode.MODEL_DESIGN)) {
                                if(rd.getDisplayName().getText().equals("InputArguments")) {
                                    System.out.println("\n"+indentationForXML+"<opc:InputArguments><!-- ... --></opc:InputArguments>");
                                } else if(rd.getDisplayName().getText().equals("OutputArguments")) {
                                    System.out.println("\n"+indentationForXML+"<opc:OutputArguments><!-- ... --></opc:OutputArguments>");
                                } else {
                                    System.out.println(MessageFormat.format(indentationForXML+"<opc:Variable SymbolicName=\"{4}\" DataType=\"ua:{3}\" TypeDefinition=\"ua:AnalogItemType\" AccessLevel=\"ReadWrite\"> <!-- NodeId: {2} --> <!-- BrowseName: {1} --> {5} </opc:Variable>", indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), rd.getDisplayName().getText(), indent.length()>0?"":("\n"+rootReference)));
                                }
                            }

                            // print list of variables for OPC UA ModelDesign (as ROOT variables)
                            if(this.getMode().equals(Mode.OPCUA_ROOT_VARIABLES))
                                System.out.println(MessageFormat.format("<opc:Variable SymbolicName=\"{4}\" DataType=\"ua:{3}\" TypeDefinition=\"ua:AnalogItemType\" AccessLevel=\"ReadWrite\"> <!-- NodeId: {2} --> <!-- BrowseName: {1} --> {5} </opc:Variable>", indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), rd.getDisplayName().getText(), rootReference));

                        }

                        else if(genericNode.getNodeClass().equals(NodeClass.Method) && this.getMode().equals(Mode.MODEL_DESIGN)) {

                            String pattern = indentationForXML+"<opc:Method SymbolicName=\"{4}\"> <!-- NodeId: {2} --> <!-- DataType: {3} --> <!-- BrowseName: {1} --> {5}";
                            System.out.print(MessageFormat.format(pattern, indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), rd.getDisplayName().getText(), indent.length()>0?"":("\n"+rootReference)));

                            if (getChildren(client, nodeId.get()).size() > 0) {

                                // recursively browse to children
                                browseChildren(indent, client, prefix, rd, last, nodeId);
                                childrenVisted = true;

                            }

                            System.out.println(indentationForXML+"</opc:Method>");
                        }

                        else if(genericNode.getNodeClass().equals(NodeClass.Object) && this.getMode().equals(Mode.MODEL_DESIGN)) {

                            String pattern = indentationForXML+"<opc:Object SymbolicName=\"{4}\" TypeDefinition=\"ua:BaseObjectType\" SupportsEvents=\"true\"> <!-- NodeId: {2} --> <!-- DataType: {3} --> <!-- BrowseName: {1} --> {5}";
                            System.out.print(MessageFormat.format(pattern, indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), rd.getDisplayName().getText(), indent.length()>0?"":("\n"+rootReference)));

                            if (getChildren(client, nodeId.get()).size() > 0) {

                                System.out.println("\n"+indentationForXML+"  <opc:Children>");

                                    // recursively browse to children
                                    browseChildren(indent, client, prefix, rd, last, nodeId);
                                    childrenVisted = true;

                                System.out.println(indentationForXML+"  </opc:Children>");

                            }

                            System.out.println(indentationForXML+"</opc:Object>");
                        }

                        else {

                            String pattern = indentationForXML+"<!-- SymbolicName: {4} --> <!-- NodeId: {2} --> <!-- DataType: {3} --> <!-- BrowseName: {1} -->";
                            System.out.println(MessageFormat.format(pattern, indent, (prefix + rd.getBrowseName().getName()).replaceAll("Objects_", ""), rd.getNodeId().toParseableString(), getType(client, rd.getNodeId()), rd.getDisplayName().getText(), indent.length()>0?"":("\n"+rootReference)));

                        }

                        if(!childrenVisted) {
                            browseChildren(indent, client, prefix, rd, last, nodeId);
                        }

                    } catch (UaException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void browseChildren(String indent, OpcUaClient client, String prefix, ReferenceDescription rd, boolean last,
            Optional<NodeId> nodeId) {
        String in = indent;
        if(indent.length() >= 3) {
            in = indent.substring(0, indent.length() - 3) + (last ? "   " : "│  ");
        }
        browseNode(in + "├──", client, nodeId.get(), prefix + rd.getBrowseName().getName() + "_");
    }

    private String randomValue(String type) {
        String value;
        switch (type) {
            case "Float":
                value = "(Float)citrus:randomNumber(1,false).citrus:randomNumber(2,false)";
                break;
            case "Double":
                value = "citrus:randomNumber(1,false).citrus:randomNumber(2,false)";
                break;
            case "Int16":
                value = "citrus:randomNumber(1,false)";
                break;
            case "Int32":
            case "Int64":
                value = "citrus:randomNumber(2,false)";
                break;
            case "Boolean":
                value = "citrus:randomEnumValue('true','false')";
                break;
            case "UtcTime":
            case "DateTime":
                value = "citrus:currentDate('yyyy-MM-dd HH:mm:ss')";
                break;
            case "String":
                value = "citrus:randomString(20)";
                break;
            default:
                value = "";
                break;
        }
        return value;
    }



    private Mode getMode() {
        if(this.envContains("mode")) {
            String mode = this.envGet("mode");

            return Mode.valueOf(mode.toUpperCase());
        }

        return Mode.CSV;
    }

}
