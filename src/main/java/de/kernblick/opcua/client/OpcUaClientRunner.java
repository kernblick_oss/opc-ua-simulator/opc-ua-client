package de.kernblick.opcua.client;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.Stack;
import org.eclipse.milo.opcua.stack.core.channel.EncodingLimits;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpcUaClientRunner {

    static {
        // Required for SecurityPolicy.Aes256_Sha256_RsaPss
        Security.addProvider(new BouncyCastleProvider());
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final CompletableFuture<OpcUaClient> future = new CompletableFuture<>();

    private final IOpcUaClient clientExample;

    public OpcUaClientRunner(IOpcUaClient clientExample) throws Exception {
        this.clientExample = clientExample;
    }

    private KeyStoreLoader getKeyStoreLoader() throws Exception {
        Path securityTempDir = Paths.get(System.getProperty("java.io.tmpdir"), "client", "security");
        Files.createDirectories(securityTempDir);
        if (!Files.exists(securityTempDir)) {
            throw new Exception("unable to create security dir: " + securityTempDir);
        }

        File pkiDir = securityTempDir.resolve("pki").toFile();

        LoggerFactory.getLogger(getClass())
            .info("security dir: {}", securityTempDir.toAbsolutePath());
        LoggerFactory.getLogger(getClass())
            .info("security pki dir: {}", pkiDir.getAbsolutePath());

        return new KeyStoreLoader().load(securityTempDir);
    }

    private EndpointDescription setEndpointUrl(EndpointDescription e, String endpointUrl) {
        return new EndpointDescription(
            endpointUrl != null ? endpointUrl : e.getEndpointUrl(),
            e.getServer(),
            e.getServerCertificate(),
            e.getSecurityMode(),
            e.getSecurityPolicyUri(),
            e.getUserIdentityTokens(),
            e.getTransportProfileUri(),
            e.getSecurityLevel());
    }

    private OpcUaClient createClient() throws Exception {

        KeyStoreLoader loader = this.getKeyStoreLoader();

        EncodingLimits limits = new EncodingLimits(
            EncodingLimits.DEFAULT_MAX_CHUNK_SIZE,
            EncodingLimits.DEFAULT_MAX_CHUNK_COUNT*4,
            EncodingLimits.DEFAULT_MAX_MESSAGE_SIZE,
            EncodingLimits.DEFAULT_MAX_RECURSION_DEPTH);
        return OpcUaClient.create(
            clientExample.getEndpointUrl(),
            endpoints ->
                endpoints.stream()
                    .filter(clientExample.endpointFilter())
                    .findFirst()
                    .map(e -> this.setEndpointUrl(e, clientExample.getEndpointUrl())),
            configBuilder ->
                configBuilder
                    .setApplicationName(LocalizedText.english("eclipse milo opc-ua client"))
                    .setApplicationUri("urn:eclipse:milo:examples:client")
                    .setIdentityProvider(clientExample.getIdentityProvider())

                    .setKeyPair(loader.getClientKeyPair())
                    .setCertificate(loader.getClientCertificate())
                    .setCertificateChain(loader.getClientCertificateChain())

                    .setRequestTimeout(uint(5000))
                    .setEncodingLimits(limits)
                    .build()
        );
    }

    public void run() {
        try {
            OpcUaClient client = createClient();

            future.whenCompleteAsync((c, ex) -> {
                if (ex != null) {
                    logger.error("Error running example: {}", ex.getMessage(), ex);
                }

                try {
                    client.disconnect().get();
                    Stack.releaseSharedResources();
                } catch (InterruptedException | ExecutionException e) {
                    logger.error("Error disconnecting: {}", e.getMessage(), e);
                }

                try {
                    Thread.sleep(1000);
                    System.exit(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

            try {
                clientExample.run(client, future);
                future.get(15, TimeUnit.SECONDS);
            } catch (Throwable t) {
                logger.error("Error running client example: {}", t.getMessage(), t);
                future.completeExceptionally(t);
            }
        } catch (Throwable t) {
            logger.error("Error getting client: {}", t.getMessage(), t);

            future.completeExceptionally(t);

            try {
                Thread.sleep(1000);
                System.exit(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            Thread.sleep(999_999_999);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}